/* ��������� ��������� �������� ���� �� �������� � ������� � �������� */
#include <stdio.h>
#define PI 3.14159
void cleanIn() 
{
 char c;
 do {       
     c = getchar();
   } 
 while (c != '\n' && c != EOF);
}

int main()
{
  float angle;
  char x;

  printf("Enter the value of an angle in radians(R) or degrees(D)\n");
  while(1)
  { 
     scanf("%f%c",&angle,&x);
     if (x=='R' || x=='D')
         break;
     else 
     {
       puts("Input error!");
       cleanIn();
     }
  }
  if (x=='D')
   {
     angle=(angle*PI)/180;
     printf("Value in radians is %.2f",angle);
   } 
  else
  {
    angle=(angle*180)/PI;
    printf("Value in degrees is %.2f",angle);
  }
  
  return(0);
 }